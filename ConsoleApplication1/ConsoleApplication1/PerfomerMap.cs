﻿using FluentNHibernate.Mapping;

namespace ConsoleApplication1
{
    class PerfomerMap : ClassMap<Perfomers>
    {
        public PerfomerMap()
        {
            Table("Perfomers");

            Id(x => x.Id)
                .GeneratedBy.Native();

            Map(x => x.Name);
          
            
            
            //HasManyToMany(x => x.Groups)
            //    .Table("UsersOnGroups")
            //    .ParentKeyColumn("UserID")
            //    .ChildKeyColumn("GroupID")
            //    .Cascade.SaveUpdate();
        }
    }
}
