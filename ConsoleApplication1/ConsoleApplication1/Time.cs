﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
   public  class Time
    {
       public int Minutes { get; set; }
       public int Seconds { get; set; }
       private bool isTime = false;
       public Time(int sec)
       {
           Seconds = sec;
           CheckTime();
       }
       public Time(int min, int sec)
        {
            Minutes = min;
            Seconds = sec;
            CheckTime();

        }
       public Time(Time time)
       {
           Minutes = time.Minutes;
           Seconds = time.Seconds;
           CheckTime();

       }
       private  Time ConvertToTime(string stime)
       {
           int minutes;
           int seconds;
           int index = stime.IndexOf(':');
           if (index > 0 && index < stime.Length)
           {
               int tmp;
               if (!int.TryParse(stime.Substring(0, index), out tmp)) Console.WriteLine("can't convert to minutes");
                minutes = tmp;
               if (!int.TryParse(stime.Substring(index + 1, stime.Length - ++index), out tmp)) Console.WriteLine("can't convert to seconds");
               seconds = tmp;
           }
           else
           {
               
               minutes = 0;
               seconds = 0;
               isTime = true;
               //Console.WriteLine("can't convert to time");
           }
           
           CheckTime();
           return new Time(minutes,seconds);

       }

       public Time(string stime)//:this(ConvertToTime(stime))
       {
           Minutes = ConvertToTime(stime).Minutes;
           Seconds = ConvertToTime(stime).Seconds;
        }

        private void CheckTime()
        {
            while (Seconds >= 60)
            {
                Minutes++;
                Seconds -= 60;
            }
        }
        public int ToSeconds()
        {
            while (Minutes >0)
            {
                Minutes--;
                Seconds += 60;
            }
            return Seconds;
        }
        public string ToString()
        {
            if (isTime) return "can't convert to time";
            return String.Format("{0}:{1:D2}", Minutes, Seconds);
        }


    }
}
