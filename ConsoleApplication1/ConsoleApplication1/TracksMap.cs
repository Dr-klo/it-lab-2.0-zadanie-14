﻿using FluentNHibernate.Mapping;

namespace ConsoleApplication1
{
    class TracksMap : ClassMap<Tracks>
    {
        public TracksMap()
        {
            Table("Tracks");

            Id(x => x.Id)
                .GeneratedBy.Native();

            Map(x => x.Name);
            Map(x => x.time);
                       
            HasManyToMany(x => x.Genres)
                .Table("GenresInTracks")
                .ParentKeyColumn("TrackID")
                .ChildKeyColumn("GanreID")
                .Cascade.SaveUpdate();
            HasManyToMany(x => x.Perfomers)
                .Table("PerfomersInTracks")
                .ParentKeyColumn("TrackID")
                .ChildKeyColumn("PerfomerID")
                .Cascade.SaveUpdate();
        }
    }
}
