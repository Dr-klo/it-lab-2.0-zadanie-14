﻿using FluentNHibernate.Mapping;

namespace ConsoleApplication1
{
    class GenreMap : ClassMap<Genre>
    {
        public GenreMap()
        {
            Table("Perfomers");

            Id(x => x.Id)
                .GeneratedBy.Native();

            Map(x => x.Name);
        }
    }
}
