﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    public class Song
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Genre { get; set; }
        public Time time { get; set; }

      public Song()
      { }
      public Song(string song)
      { CreateNewSong(song); }
      public void SetTime(string p)
    {
        time = new Time(p);
    }
      public string ToString()
    {

        return String.Format("{0,3}. {1,15} - {2,25},{3,5},{4}",Id, Author, Name, time.ToString(), Genre); 
    }
      public void SetGenre(string p)
    {
        Genre = p;
    }
      public void SetAuthor(string p)
    {
        Author = p;
    }
      public void SetName(string p)
    {
        Name = p;
    }
      public void SetId(string p)
    { int tmp;
       int.TryParse(p,out tmp);
       Id = tmp;
    }

    public bool Compare(Song song)
    {
        bool result=true;
        string tmp1,tmp2;
        tmp1 = this.Name;
        tmp2 = song.Name;
        if (!(String.Compare(tmp1.ToLower(), tmp2.ToLower()) == 0)) result= false;
        tmp1 = this.Author;
        tmp2 = song.Author;
        if (!(String.Compare(tmp1.ToLower(), tmp2.ToLower()) == 0)) result = false; 
        return result;
    }
    private  void CreateNewSong(string line)
    {
        int lastpos = 0;
        SetId(readItem('.', ref line, ref lastpos));
        SetName(readItem('-', ref line, ref lastpos));
        SetAuthor(readItem(';', ref line, ref lastpos));
        SetTime(readItem(';', ref line, ref lastpos));
        SetGenre(readItem('~', ref line, ref lastpos));
        
    }

    private static string readItem(char item, ref string line, ref int lastpos)//было бы неплохо вынести этот метод в отдельный класс
    {
        string tmp = null;
        int currpos = line.IndexOf(item, lastpos);
        if (item == '~') currpos = line.Length;
        if (currpos > 0) tmp = line.Substring(lastpos, currpos - lastpos);
        lastpos = ++currpos;
        return tmp;
    }

    }
}