﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System.Reflection;
using NHibernate.Tool.hbm2ddl;
using System.Threading.Tasks;


namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //using (ISession session = sessionFactory.OpenSession())
            //{
            //    var users = session.QueryOver<User>().List();

            //    foreach (var user in users)
            //    {
            //        Console.WriteLine(user.UserName);

            //        foreach (var userGroup in user.Groups)
            //        {
            //            Console.WriteLine("  {0}", userGroup.Name);
            //        }
            //    }

            //    using (var transaction = session.BeginTransaction())
            //    {
            //        var newGroup = new UserGroup
            //        {
            //            Name = "New Group 1"
            //        };

            //        var user = session.Get<User>(5);

            //        user.Groups.Add(newGroup);

            //        transaction.Commit();
            //    }
            //}

            var song = new Song();
            List<Song> songs = new List<Song> { };

            string[] lines = File.ReadAllLines(@"C:\music.txt");

            foreach (var line in lines)
            {
                songs.Add(new Song(line));
            }
            List<Tracks> track = new List<Tracks>();
            foreach (var tmp in songs)
            {
                track.Add(new Tracks(tmp));
            }
            foreach (var tmp in songs)
            Console.WriteLine(tmp.ToString());
            Console.WriteLine("\n");
            foreach (var tmp in songs)
            Console.WriteLine("{0,-20} vs {1,10}  res:{2,7}", tmp.Name, songs[3].Name, songs[3].Compare(tmp));


            FluentConfiguration config = Fluently
                  .Configure()
                  .Database(MsSqlConfiguration
                                .MsSql2008
                                .ConnectionString(@"Data Source=.\SQLEXPRESS;Initial Catalog=NEWDB;Integrated Security=True;Pooling=False"))
                                .Mappings(configuration => configuration
                                    .FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()));

            var export = new SchemaUpdate(config.BuildConfiguration());
            export.Execute(true, true);

            ISessionFactory sessionFactory = config.BuildSessionFactory();
        }


        
    }
}
