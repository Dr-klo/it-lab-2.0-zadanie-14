﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    public class Tracks
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<Perfomers> Perfomers { get; set; }
        public virtual int time { get; set; }
        public virtual IList<Genre> Genres { get; set; }
        public Tracks() { }
        public Tracks(Song song)
        {
            Id = song.Id;
            Name = song.Name;
            time = song.time.ToSeconds();
            Perfomers = GetPerfomers(song.Author);
            Genres = GetGenres(song.Genre);
        }

        private IList<Perfomers> GetPerfomers(string p)
        {   
            List<Perfomers> perf=new List<Perfomers>();
            int lastpos=0;
            string item=readItem(',', ref p, ref lastpos);
            while ( item!= null)
            {
                perf.Add(new Perfomers(item));
                item = readItem(',', ref p, ref lastpos);
            }
            return perf;
        }
        //private bool Test(int i)
        //{
        //    while (i > 0)
        //    {//do smth
        //    }
        //    return true;
        //}

        private IList<Genre> GetGenres(string p)
        {
            List<Genre> genr = new List<Genre>();
            int lastpos = 0;
            string item = readItem(',', ref p, ref lastpos);
            while (item != null)
            {
                genr.Add(new Genre(item));
                item = readItem(',', ref p, ref lastpos);
            }
            return genr;
        }
        private static string readItem(char item, ref string line, ref int lastpos)//было бы неплохо вынести этот метод в отдельный класс
        {
            string tmp = null;
            int currpos = line.IndexOf(item, lastpos);
            if (item == '~') currpos = line.Length;
            if (currpos > 0) tmp = line.Substring(lastpos, currpos - lastpos);
            lastpos = ++currpos;
            return tmp;
        }
    }
}
